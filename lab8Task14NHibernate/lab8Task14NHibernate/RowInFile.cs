﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab8Task14NHibernate
{
    class RowInFile
    {
        public String NameTrack {  get;set; }
        public String NameArtist { get; set; }
        public String NameAlbum { get; set; }
        public int LenghtTrack { get; set; }
        public List<String> ListGenre  { get; set; }
        public String Row { get; set; }
        public int[] masIndex;

        public RowInFile(string str) 
        {
            Row = str;
            masIndex = new int[4] { Row.IndexOf('.'), Row.IndexOf('-'), Row.IndexOf('('), Row.IndexOf(')') };
            int lenghtString = Row.Length;
            this.NameTrack = Row.Split(';')[0].Substring(masIndex[0] + 2, masIndex[1] - masIndex[0] - 2);
            if (IsAlbum())
            {
                this.NameArtist = Row.Substring(masIndex[1] + 2, masIndex[2] - masIndex[1] - 2);
                this.NameAlbum = Row.Substring(masIndex[2] + 1, masIndex[3] - masIndex[2] - 1);
            }
            else
            {
                this.NameArtist = Row.Split(';')[0].Substring(masIndex[1] + 2);
            }
            this.LenghtTrack = ConvertTime(Row.Split(';')[1]);
            ListGenre = new List<String>();
            foreach (var genr in Row.Split(';')[2].Split(','))
            {
                this.ListGenre.Add(genr.ToString());
            }
        }

        public int ConvertTime(string timeStr)
        {
            return int.Parse(timeStr.Split(':')[0])*60+
                int.Parse(timeStr.Split(':')[1]);
        }

        public bool IsAlbum()
        {
            if (Row.IndexOf('(')!=-1&&Row.IndexOf(')')!=-1)
            {
                return true;
            }
            else{   
                return false;
            }
        }
    }
}
