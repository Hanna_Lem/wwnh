﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FluentNHibernate.Mapping;



namespace lab8Task14NHibernate
{
    class GenreMap : ClassMap<Genre>
    {
        public GenreMap()
        {
            Table("genre");

            Id(x => x.Id)
                .GeneratedBy.Native();

            Map(x => x.Name);

            HasManyToMany(x => x.Tracks)
               .Cascade.SaveUpdate();

        }
    }
}
