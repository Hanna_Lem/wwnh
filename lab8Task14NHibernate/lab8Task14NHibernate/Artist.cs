﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab8Task14NHibernate
{
    public class Artist
    {
        public virtual int Id{set;get;}
        public virtual string Name { set; get; }

        public virtual IList<Album> Albums { get; set; }
        public virtual IList<Track> Tracks { get; set; }

        public Artist()
        {
            Albums = new List<Album>();
            Tracks = new List<Track>();
        }

        public Artist(int id, string name)
        {
            Albums = new List<Album>();
            Tracks = new List<Track>();
            this.Id = id;
            this.Name = name;
        }

        

    }
}
