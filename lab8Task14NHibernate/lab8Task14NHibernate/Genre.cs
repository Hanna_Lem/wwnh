﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab8Task14NHibernate
{
    public class Genre
    {
        public virtual int Id{set;get;}
        public virtual string Name { set; get; }
        public virtual IList<Track> Tracks { get; set; }

        public Genre() {
            Tracks = new List<Track>();
        }

        public Genre(int id, string name)
        {
            Tracks = new List<Track>();
            this.Id = id;
            this.Name = name;
        }
    }
}
