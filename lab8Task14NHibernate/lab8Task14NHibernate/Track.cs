﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab8Task14NHibernate
{
    public class Track
    {
        public virtual int IdTrack{set;get;}
        public virtual string Name{set;get;}
        public virtual int Lenght { set; get; }

        public virtual Artist Artist { set; get; }

        public virtual IList<Album> Albums { set; get; }
        public virtual IList<Genre> Genres { set; get; }

        public Track() {
            Genres = new List<Genre>();
            Albums = new List<Album>();
        }

        public Track(int id, string name, int lenght)
        {
            Genres = new List<Genre>();
            Albums = new List<Album>();
            this.IdTrack = id;
            this.Name = name;
            this.Lenght = lenght;
        }
    }
}
