﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;



namespace lab8Task14NHibernate
{
    public class WorkWithDB
    {
        public static  ISessionFactory BuildSessionFactory()
        {
            FluentConfiguration conf = Fluently
                .Configure()
                .Database(MsSqlConfiguration
                              .MsSql2008
                              .ConnectionString(@"Data Source=ANUTA-PC;Initial Catalog=NewDB2;Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=False"))
                .Mappings(configuration => configuration.FluentMappings.AddFromAssembly(Assembly.GetEntryAssembly()));

            var export = new SchemaUpdate(conf.BuildConfiguration());
            export.Execute(true, true);

            return conf.BuildSessionFactory();
        }
    }
}
