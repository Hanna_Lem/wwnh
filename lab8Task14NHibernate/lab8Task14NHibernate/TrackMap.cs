﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FluentNHibernate.Mapping;

namespace lab8Task14NHibernate
{
    class TrackMap : ClassMap<Track>
    {
        public TrackMap()
        {
            Table("track");

            Id(x => x.IdTrack)
                .GeneratedBy.Native();

            Map(x => x.Name);
            Map(x => x.Lenght);

            References(x => x.Artist);

            HasManyToMany(x => x.Genres)
                .Table("TrackOnGenre")
                .Cascade.SaveUpdate();

            HasManyToMany(x => x.Albums)
                .Table("AlbumOnTrack")
                .Cascade.SaveUpdate();
        }
    }
}
