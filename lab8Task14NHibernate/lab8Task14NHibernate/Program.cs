﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


using NHibernate;
using NHibernate.Criterion;


namespace lab8Task14NHibernate
{
    class Program
    {
        static void Main(string[] args)
        {
           

            ISessionFactory sessionFactory = WorkWithDB.BuildSessionFactory();

            //////////////////////////////////////////////

            StreamReader streamReader = new StreamReader("music.txt");

            /////////////////////////////////////////////

            List<RowInFile> listRowInFile = new List<RowInFile>();

            while (!streamReader.EndOfStream)
            {
                listRowInFile.Add(new RowInFile(streamReader.ReadLine()));
            }

            //////////////////////////////////////

            using (ISession session = sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    for (int i = 0; i < listRowInFile.Count; i++)
                    {
                        Track track = session.QueryOver<Track>()
                            .Where(x => x.Name == listRowInFile[i].NameTrack)
                            .JoinQueryOver<Artist>(x => x.Artist)
                            .Where(x => x.Name == listRowInFile[i].NameArtist)
                            .List()
                            .FirstOrDefault();

                        if (track == null)
                        {
                            track = new Track
                            {
                                Name = listRowInFile[i].NameTrack,
                                Lenght = listRowInFile[i].LenghtTrack
                            };
                        }
                        else
                        {
                            if (track.Lenght != listRowInFile[i].LenghtTrack)
                            {
                                track.Lenght = listRowInFile[i].LenghtTrack;
                            }
                        }

                        foreach (String gen in listRowInFile[i].ListGenre)
                        {
                            Genre genre = session.QueryOver<Genre>()
                                .Where(x => x.Name == gen)
                                .List()
                                .FirstOrDefault();

                            if (genre == null)
                            {

                                genre = new Genre
                                {
                                    Name = gen
                                };
                            }
                            session.SaveOrUpdate(genre);

                            if (!track.Genres.Any(x => x.Name == genre.Name))//переделать под БД
                            {
                                track.Genres.Add(genre);
                            }
                        }

                       

                        Artist artist = session.QueryOver<Artist>()
                                .Where(x => x.Name == listRowInFile[i].NameArtist)
                                .List()
                                .FirstOrDefault();

                        if (artist == null)
                        {

                            artist = new Artist
                            {
                                Name = listRowInFile[i].NameArtist
                            };
                        }

                        if (listRowInFile[i].NameAlbum != null)
                        {
                            Album album = session.QueryOver<Album>()
                                .Where(x => x.Name == listRowInFile[i].NameAlbum)
                                .List()
                                .FirstOrDefault();

                            if (album == null)
                            {
                                album = new Album
                                {
                                    Name = listRowInFile[i].NameAlbum
                                };
                            }

                            if (!album.Tracks.Any(x => x.Name == track.Name))//переделать под БД
                            {
                                album.Tracks.Add(track);
                            }
                        session.SaveOrUpdate(album);
                        album.Artist = artist;

                        }
                        session.SaveOrUpdate(track);

                        track.Artist = artist;



                        session.SaveOrUpdate(artist);
                    }
                    transaction.Commit();
                }

                /////////////////////////////////////////////////////////

                Console.WriteLine(" +Альбомы гуппы Linkin Park");
                var q1 = session.QueryOver<Album>()
                    .JoinQueryOver<Artist>(x => x.Artist)
                    .Where(y => y.Name == "Linkin Park")
                    .List();

                foreach (var q in q1)
                {
                    Console.WriteLine(" - " + q.Name);               
                }

                //////////////////////////////////////////////////////

                Console.WriteLine("\n +Песни из альбомов гуппы Linkin Park: ");

                var q2 = session.QueryOver<Track>()
                    .JoinQueryOver<Album>(x => x.Albums)
                    .JoinQueryOver<Artist>(x => x.Artist)
                    .Where(y => y.Name == "Linkin Park")
                    .List();

                foreach (var q in q2)
                {
                    Console.WriteLine("- " + q.Name + " /" + q.Lenght);
                }

                ////////////////////////////////////////////////

                Console.WriteLine("\n Пары групп с одноименными песнями: ");

                //выводит название песни, и количество исполнителей, которые её поют

                var q3 = session.QueryOver<Track>()
                .Select(Projections.GroupProperty
                 (Projections.Property<Track>(x => x.Name))
                 ,Projections.Count<Track>(x => x.Name))
                 .Where(Restrictions.Gt(Projections.Count<Track>(x => x.Name), 1))
                 .List<object[]>();                 
                
                foreach (var q in q3)
                {
                    Console.WriteLine(" - " + q[0] + " " + q[1]);
                    //Console.WriteLine("- " + q.Name + " /" + q.Lenght);
                }

                ////////////////////////////////////////////////
                Console.WriteLine("\n +Песни группы HannaLem с длительностью больше lenght1 = 180: ");

                int lenght1 = 180;

                var q4 = session.QueryOver<Track>()
                    .Where(x => x.Lenght > lenght1)
                    .JoinQueryOver<Artist>(x => x.Artist)
                    .Where(y => y.Name == "HannaLem")
                    .List();

                foreach (var q in q4)
                {
                    Console.WriteLine("- " + q.Name + " /" + q.Lenght);
                }

                ////////////////////////////

                Console.WriteLine("\n +Песни из альбома  ABC с длительностью меньше lenght1 = 180: ");
                
                var q5 = session.QueryOver<Track>()
                    .Where(x => x.Lenght < lenght1)
                    .JoinQueryOver<Album>(x => x.Albums)
                    .Where(y => y.Name == "ABC")
                    .List();
                
                foreach (var q in q5)
                {
                    Console.WriteLine("- " + q.Name + " /" + q.Lenght);
                }

                ///////////////////////////

                Console.WriteLine("\n +Песни жанра Metal: ");

                String ganreName = "Metal";
                var q6Tracks = session.QueryOver<Track>()
                    .JoinQueryOver<Genre>(x => x.Genres)
                    .Where(x => x.Name == ganreName)
                    .List();
                    
                foreach (var track in q6Tracks)
                {
                    Console.WriteLine(" - " + track.Name + " /" + track.Lenght);
                }

                //////////////////////////////////////

                Console.WriteLine("\n Песни являющиеся одновременно песнями жанров Alternative Rock,Rock: ");
                
                // выводит id + количество жанров у песни

                String[] ganresName = { "Alternative Rock", "Rock" };
                
                var q7 = session.QueryOver<Track>()
                    .JoinQueryOver<Genre>(x => x.Genres)
                    .Select(Projections.GroupProperty
                    (Projections.Property<Track>(x => x.IdTrack)),
                    Projections.Count<Track>(x => x.Name))
                    //.Where(Restrictions.Gt(Projections.Count<Track>(x => x.Name), 1))
                    .List<object[]>();
                    //.List();
                
                   /* IList<Track> r12 = session.QueryOver<Track>()
                   .JoinQueryOver<Genre>(x => x.Genres)
                   .Where(Restrictions.On<Genre>(x => x.Name).IsLike(ganresName[0]))
                   .Where(Restrictions.On<Genre>(x => x.Name).IsLike(ganresName[1]))
                   .List<Track>();  */
                    
                foreach (var track in q7)
                {
                    Console.WriteLine(" - " + track[0] + " " + track[1]);
                     //Console.WriteLine(" - " + track.Name + " /" + track.Lenght);
                }

                    ////////////////////////////////////

                Console.WriteLine("\n +Все песни, являющиеся одним из жанров  Alternative Rock,Metal: ");

                String[] ganresName2 = { "Alternative Rock", "Metal" };

                IList<Track> q8Tracks = session.QueryOver<Track>()
                   .JoinQueryOver<Genre>(x => x.Genres)
                   .Where(Restrictions.On<Genre>(x => x.Name).IsIn(ganresName2))
                   .List<Track>();

                foreach (var track in q8Tracks)
                {
                    Console.WriteLine(" - " + track.Name + " /" + track.Lenght);
                }

                ///////////////////////////////

                Console.WriteLine("\n Все альбомы с указанием преобладающего жанра (жанров, если количество песен совпадает): ");


                //////////////////////

                Console.WriteLine("\n +Жанр, преобладающий во всем трэк-листе: ");

                var q10 = session.QueryOver<Genre>()
                  .JoinQueryOver<Track>(x => x.Tracks)
                  .Select(Projections.GroupProperty
                  (Projections.Property<Genre>(x => x.Name))
                  , Projections.Count<Genre>(x => x.Name))
                  //.Select(Projections.Max(
                  //Projections.Count<Genre>(x => x.Name)))
                    .OrderBy(Projections.Count<Genre>(x => x.Name)).Desc
                    .Take(1)
                    .List<object[]>()
                  ;

                //Console.WriteLine(" - " + r11);
                foreach (var t in q10)
                {
                    Console.WriteLine(" - " + t[0]);
                }
                /////////////////////////////////////


                Console.WriteLine("\n +Рейтинг жанров трэк-листа начиная с самого распространенного: ");
                
                var q11 = session.QueryOver<Genre>()
                    .JoinQueryOver<Track>(x => x.Tracks)
                    .Select(Projections.GroupProperty
                    (Projections.Property<Genre>(x => x.Name))
                    , Projections.Count<Genre>(x => x.Name))
                    .OrderBy(Projections.Count<Genre>(x => x.Name)).Desc
                    .List<object[]>();

                foreach (var t in q11)
                {
                    Console.WriteLine(" - " + t[0] + " " + t[1]);
                }

                //////////////////////////////////////////
                Console.WriteLine("\n Песни уникального жанра (только 1 песня данного жанра): ");

                // выводит жанр, повторяющийся только 1 раз

                var q12 = session.QueryOver<Genre>()
                   .JoinQueryOver<Track>(x => x.Tracks)
                   .Select(Projections.GroupProperty
                   (Projections.Property<Genre>(x => x.Name))
                   , Projections.Count<Genre>(x => x.Name))
                   .Where(Restrictions.Eq(Projections.Count<Genre>(x => x.Name), 1))
                   .List<object[]>();

                foreach (var t in q12)
                {
                    Console.WriteLine(" - " + t[0] + " " + t[1]);
                }           

                ////////////////////////////////////////////////

                Console.WriteLine("\n +Количество песен, не содержащих жанр Rock: ");
                var q13 =
                    session.QueryOver<Track>()
                    .RowCount()
                    -
                     session.QueryOver<Track>()
                    .JoinQueryOver<Genre>(x => x.Genres)
                    .Where(x => x.Name == "Rock")
                    .RowCount();     
                
                Console.WriteLine(" n = " + q12);

                ///////////////////////////////////
                
                Console.WriteLine("\n +Количество песен, не лежащих в альбомах: ");


                var q14 =
                     session.QueryOver<Track>()
                     .RowCount()
                     - 
                     session.QueryOver<Track>()
                    .JoinQueryOver<Album>(x => x.Albums)
                    .Select(Projections.GroupProperty
                    (Projections.Property<Track>(x => x.Name))
                    , Projections.Count<Track>(x => x.Name))
                    .RowCount();                          

                Console.WriteLine(" n = " + q14);
                
            }
            Console.ReadLine();
        }
    }
}
