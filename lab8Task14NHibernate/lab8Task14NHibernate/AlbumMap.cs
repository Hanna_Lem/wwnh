﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FluentNHibernate.Mapping;

namespace lab8Task14NHibernate
{
    class AlbumMap : ClassMap<Album>
    {
        public AlbumMap()
        {
            Table("album");

            Id(x => x.Id)
                .GeneratedBy.Native();

            Map(x => x.Name);
            References(x => x.Artist);

            HasManyToMany(x => x.Tracks)
                .Table("AlbumOnTrack")
                .Cascade.SaveUpdate();
        }
    }
}
