﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;

namespace lab8Task14NHibernate
{
    class ArtistMap: ClassMap<Artist>
    {
        public ArtistMap()
        {
            Table("artist");

            Id(x => x.Id)
                .GeneratedBy.Native();

            Map(x => x.Name);

            HasMany<Album>(x => x.Albums)
                      .Cascade.SaveUpdate();

            HasMany<Track>(x => x.Tracks)
                      .Cascade.SaveUpdate();
        }
    }
}
